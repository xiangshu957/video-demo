pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
        maven { setUrl("https://jitpack.io") }
        maven { setUrl("https://repo.spring.io/plugins-release/") }
        maven { setUrl("https://maven.aliyun.com/repository/public") }
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven { setUrl("https://jitpack.io") }
        maven { setUrl("https://repo.spring.io/plugins-release/") }
        maven { setUrl("https://maven.aliyun.com/repository/public") }
    }
}

rootProject.name = "MyApplication"
include(":app")
