package com.example.myapplication

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import com.shuyu.gsyvideoplayer.cache.CacheFactory
import com.shuyu.gsyvideoplayer.cache.ProxyCacheManager
import com.shuyu.gsyvideoplayer.player.IjkPlayerManager
import com.shuyu.gsyvideoplayer.player.PlayerFactory
import com.shuyu.gsyvideoplayer.player.SystemPlayerManager
import com.shuyu.gsyvideoplayer.video.GSYSampleADVideoPlayer

class VideoActivity : AppCompatActivity() {

    private val videoPlayer: GSYSampleADVideoPlayer by lazy { findViewById(R.id.video_player) }
    private val etVideoUrl: AppCompatEditText by lazy { findViewById(R.id.et_video_url) }
    private val btnChange: Button by lazy { findViewById(R.id.btn_change) }
    private val btnUp: Button by lazy { findViewById(R.id.btn_up) }
    private val btnDown: Button by lazy { findViewById(R.id.btn_down) }
    private val btnLeft: Button by lazy { findViewById(R.id.btn_left) }
    private val btnRight: Button by lazy { findViewById(R.id.btn_right) }


    @SuppressLint("MissingInflatedId", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        //系统内核模式
        PlayerFactory.setPlayManager(SystemPlayerManager::class.java)
        //ijk内核，默认模式
        PlayerFactory.setPlayManager(IjkPlayerManager::class.java)

        //代理缓存模式，支持所有模式，不支持m3u8等，默认
        CacheFactory.setCacheManager(ProxyCacheManager::class.java)

        //切换渲染模式
//        GSYVideoType.setShowType(GSYVideoType.SCREEN_MATCH_FULL)

        etVideoUrl.setText("rtsp://192.168.1.4:554/main")

        videoPlayer.setAdUp(
            arrayListOf(
                GSYSampleADVideoPlayer.GSYADVideoModel(
                    etVideoUrl.text.toString(),
                    "",
                    GSYSampleADVideoPlayer.GSYADVideoModel.TYPE_NORMAL
                )
            ), false, 0
        )

        btnChange.setOnClickListener {
            videoPlayer.onVideoReset()
            videoPlayer.setAdUp(
                arrayListOf(
                    GSYSampleADVideoPlayer.GSYADVideoModel(
                        etVideoUrl.text.toString(),
                        "",
                        GSYSampleADVideoPlayer.GSYADVideoModel.TYPE_NORMAL
                    )
                ), false, 0
            )
        }

        btnUp.setOnClickListener {
            operation("上")
        }

        btnDown.setOnClickListener {
            operation("下")
        }

        btnLeft.setOnClickListener {
            operation("左")
        }

        btnRight.setOnClickListener {
            operation("右")
        }


    }

    fun operation(str: String) {
        Toast.makeText(this, str, Toast.LENGTH_LONG).show()
    }
}