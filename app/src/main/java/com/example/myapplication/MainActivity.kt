package com.example.myapplication

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.example.myapplication.ui.theme.MyApplicationTheme
import com.shuyu.gsyvideoplayer.GSYVideoManager
import com.shuyu.gsyvideoplayer.cache.CacheFactory
import com.shuyu.gsyvideoplayer.cache.ProxyCacheManager
import com.shuyu.gsyvideoplayer.player.IjkPlayerManager
import com.shuyu.gsyvideoplayer.player.PlayerFactory
import com.shuyu.gsyvideoplayer.player.SystemPlayerManager
import com.shuyu.gsyvideoplayer.video.GSYSampleADVideoPlayer
import com.shuyu.gsyvideoplayer.video.GSYSampleADVideoPlayer.GSYADVideoModel.TYPE_NORMAL

class MainActivity : ComponentActivity() {

    //    var command = arrayOf(
//        "ffmpeg",
//        "-i", "rtsp://192.168.1.4:554/main",
//        "-acodec", "copy",
//        "-vcodec", "copy",
//        "-f", "mpegts", "output.ts"
//    )
    var command = arrayOf(
        "ffmpeg -i \" + rtsp://192.168.1.4:554/main + \" -f s16le -ar 44100 -ac 2 -vn output.pcm"
    )

    private var videoUrl = "rtsp://192.168.1.4:554/main"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            // A surface container using the 'background' color from the theme
            Surface(
                modifier = Modifier.fillMaxSize(),
                color = MaterialTheme.colorScheme.background
            ) {

                Greeting(this, videoUrl)
            }
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun Greeting(context: Context, videoUrl: String, modifier: Modifier = Modifier) {
//        val videoUrlState = rememberSaveable {
//            mutableStateOf(videoUrl)
//        }
        var videoUrlState by rememberSaveable {
            mutableStateOf(videoUrl)
        }
        //系统内核模式
        PlayerFactory.setPlayManager(SystemPlayerManager::class.java)
        //ijk内核，默认模式
        PlayerFactory.setPlayManager(IjkPlayerManager::class.java)

        //代理缓存模式，支持所有模式，不支持m3u8等，默认
        CacheFactory.setCacheManager(ProxyCacheManager::class.java)

        //切换渲染模式
//        GSYVideoType.setShowType(GSYVideoType.SCREEN_MATCH_FULL)


        Column(verticalArrangement = Arrangement.spacedBy(8.dp)) {

            AndroidView(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(200.dp),
                factory = { context ->
                    val inflater = LayoutInflater.from(context)
                    val frameLayout = FrameLayout(context) as ViewGroup
                    val videoView = inflater.inflate(
                        R.layout.layout_video_view,
                        frameLayout,
                        false
                    ) as GSYSampleADVideoPlayer
                    videoView.setAdUp(
                        arrayListOf(
                            GSYSampleADVideoPlayer.GSYADVideoModel(
                                videoUrlState,
                                "",
                                TYPE_NORMAL
                            )
                        ), false, 0
                    )
                    frameLayout.addView(videoView)
                    frameLayout
                },
                update = {
                    // 在这里可以更新视图，如果需要的话

                }
            )

            TextField(value = videoUrlState, onValueChange = { videoUrlState = it }, modifier = Modifier)

            Row(horizontalArrangement = Arrangement.spacedBy(8.dp), modifier = Modifier
                .weight(1F)
                .fillMaxWidth()) {

                Button(onClick = {

                    videoUrlState = "上"

                }) {
                    Text(text = "上")
                }

                Button(onClick = {  }) {
                    Text(text = "下")
                }

                Button(onClick = {  }) {
                    Text(text = "左")
                }

                Button(onClick = {  }) {
                    Text(text = "右")
                }

            }

        }


    }

}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    MyApplicationTheme {
//        Greeting("Android")
    }
}